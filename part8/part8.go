package part8

// We add a new dependency on the errors package which gives us convenience functions such as `wrap` and `cause`
import (
	"fmt"
	"github.com/pkg/errors"
	"runtime"
	"time"
)

type work func(...int) int

type job struct {
	w    work
	args []int
}

type Scheduler struct {
	maxgr   int
	timeout time.Duration
	jobs    []job
}

func NewScheduler(maxgr int, timeout time.Duration) *Scheduler {
	if maxgr == 0 {
		maxgr = runtime.NumCPU()
	}

	return &Scheduler{
		maxgr:   maxgr,
		timeout: timeout,
	}
}

func (s *Scheduler) Add(w work, args ...int) {
	s.jobs = append(s.jobs, job{w, args})
}

type worker struct {
	job
	position int
}

type workStream chan worker

type workResult struct {
	value,
	position int
	err error
}

type resultStream chan workResult

func doWork(ws workStream, rs resultStream, cancel <-chan struct{}, timeout time.Duration) {
	for {
		select {
		case worker := <-ws:
			workStream := make(chan int, 1)

			go func() {
				// To recover from a panic you need to defer the check to the end of the panicked goroutine
				// See https://golang.org/doc/effective_go.html#recover for details
				defer func() {
					// We check if we've recovered from a panic and wrap the message in our custom panic error
					// Technically we should check if we can type r to a string...
					if r := recover(); r != nil {
						rs <- workResult{
							0,
							worker.position,
							wrapPanic(fmt.Errorf(r.(string))),
						}
					}
				}()
				defer close(workStream)

				workStream <- worker.w(worker.args...)
			}()

			select {
			// We now have to check that we're not simply reading a zero value from `workStream` when it's closed
			// When we panic we've already written to the result stream so can skip this bit
			case r, ok := <-workStream:
				if ok {
					rs <- workResult{
						r,
						worker.position,
						nil,
					}
				}
			case <-time.After(timeout):
				rs <- workResult{
					0,
					worker.position,
					Timeout,
				}
			}
		case <-cancel:
			return
		}
	}
}

type Result struct {
	Value int
	Err   error
}

var Timeout = errors.New("Timeout")

// This convenience function allows us to clearly decorate the handled error and say it was of this "type"
func wrapPanic(err error) error {
	return errors.Wrap(err, "Panicked")
}

func (s *Scheduler) Run() []Result {
	var todo = make([]job, len(s.jobs))
	copy(todo, s.jobs)
	s.jobs = make([]job, 0)

	totalWork := len(todo)
	results := make([]Result, totalWork)
	workStreams := make([]workStream, 0)
	currentWorker := 0
	rs := make(resultStream, totalWork)
	cancelWorkers := make(chan struct{})

	defer close(rs)
	defer close(cancelWorkers)

	for i, j := range todo {
		if len(workStreams) < s.maxgr {
			ws := make(workStream)
			workStreams = append(workStreams, ws)
			go doWork(ws, rs, cancelWorkers, s.timeout)
		}

		if currentWorker == len(workStreams) {
			currentWorker = 0
		}

		workStreams[currentWorker] <- worker{j, i}

		currentWorker++
	}

	for i := 0; i < totalWork; i++ {
		r := <-rs
		results[r.position] = Result{
			r.value,
			r.err,
		}
	}

	return results
}
