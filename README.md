# Go tutorial

This repo is split into eight parts with various goals in order to learn
the fundamentals of Go. We'll be building a lazy scheduler, adding
capabilities along the way and fixing any weak points or looking at
alternative approaches.

Each part has a `go.mod` file so that it can be treated as though it were
a root package. The code is annotated to draw attention to interesting
parts of Go or a certain pattern.

You can approach this in a couple of ways. Either trying to implement
the necessary code and tests by using the README in each part and/or
copying the test code and then try and get them to pass.

It's advised to work through each part in order as they're built sequentially.

# Basic goal

We want a package that we can lazily schedule functions with (they're not called when we schedule them), with their
arguments, and then be able to execute them at a future point in time. The results returned at the point of execution,
should be in the same order in which we scheduled the functions.

For simplicity the signature of the functions we can schedule should be `func(...int) int`

From here we'll also add new functionality and perhaps change requirements
in later parts.
