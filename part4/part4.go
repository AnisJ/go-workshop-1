package part4

import "runtime"

type work func(...int) int

type job struct {
	w    work
	args []int
}

type Scheduler struct {
	maxgr int
	jobs  []job
}

func NewScheduler(maxgr int) *Scheduler {
	if maxgr == 0 {
		// if we haven't been provided with a maximum value then default to the number of cores
		maxgr = runtime.NumCPU()
	}

	return &Scheduler{
		maxgr: maxgr,
	}
}

func (s *Scheduler) Add(w work, args ...int) {
	s.jobs = append(s.jobs, job{w, args})
}

type worker struct {
	// You can compose `struct`s and reference the flattened properties or the name of the nested `struct`
	// See https://golang.org/doc/effective_go.html#embedding for details
	job
	position int
}

type workStream chan worker

type result struct {
	value,
	position int
}

type resultStream chan result

func doWork(ws workStream, rs resultStream) {
	for {
		select {
		case worker := <-ws:
			rs <- result{
				worker.w(worker.args...),
				worker.position,
			}
		}
	}
}

func (s *Scheduler) Run() []int {
	totalWork := len(s.jobs)
	results := make([]int, totalWork)
	// In this approach we create a slice of channels which we'll use to orchestrate across our limited number of
	// goroutines. See https://golang.org/doc/effective_go.html#channels for a starter on channels
	workStreams := make([]workStream, 0)
	// Here we track which goroutine we'd like to send work to in a round robin style
	currentWorker := 0
	// We fan back in on this channel which we know the capacity of so buffer it with the expected number of results
	// which avoids blocking on the writes
	rs := make(resultStream, totalWork)

	// The defer keyword will close the result stream channel when the `Run` method finishes regardless of what has
	// happened even a panic
	defer close(rs)

	for i, j := range s.jobs {
		// Keep spinning up goroutines until we've reached our maximum number
		if len(workStreams) < s.maxgr {
			ws := make(workStream)
			workStreams = append(workStreams, ws)
			go doWork(ws, rs)
		}

		// Round robin our current workstream
		if currentWorker == len(workStreams) {
			currentWorker = 0
		}

		// Send this piece of work to the next goroutine
		workStreams[currentWorker] <- worker{j, i}

		// Move to the next workstream
		currentWorker++
	}

	// We know how much work we have to do so can use a fixed loop to read each of the results
	for i := 0; i < totalWork; i++ {
		// This read will block if nothing has been written to the results stream
		r := <-rs
		// Because we own the results slice we write to the specific position without a mutex
		results[r.position] = r.value
	}

	return results
}
