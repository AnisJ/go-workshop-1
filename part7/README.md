# Part 7 - execute many

You may already have implemented this but so far in this repo, if you
schedule and execute many times we haven't addressed the cumulative
nature of our schedule

## Goal

* Add the ability to get back from your execution only the results
from the functions that have been scheduled prior and after the last
execution (unless it's the first execution)


## Acceptance criteria

* Confirm you can schedule and execute many times with the correct results
received for each tranche of work

## Learning outcomes

* copy
