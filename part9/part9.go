package part9

import (
	"context"
	"fmt"
	"github.com/pkg/errors"
	"runtime"
	"time"
)

type work func(...int) int

type job struct {
	w    work
	args []int
}

type Scheduler struct {
	maxgr   int
	timeout time.Duration
	jobs    []job
}

func NewScheduler(maxgr int, timeout time.Duration) *Scheduler {
	if maxgr == 0 {
		maxgr = runtime.NumCPU()
	}

	return &Scheduler{
		maxgr:   maxgr,
		timeout: timeout,
	}
}

func (s *Scheduler) Add(w work, args ...int) {
	s.jobs = append(s.jobs, job{w, args})
}

type worker struct {
	job
	position int
}

type workStream chan worker

type workResult struct {
	value,
	position int
	err error
}

type resultStream chan workResult

func doWork(ws workStream, rs resultStream, cancel <-chan struct{}, timeout time.Duration) {
	for {
		select {
		case worker := <-ws:
			workStream := make(chan int, 1)

			go func() {
				defer func() {
					if r := recover(); r != nil {
						rs <- workResult{
							0,
							worker.position,
							wrapPanic(fmt.Errorf(r.(string))),
						}
					}
				}()
				defer close(workStream)

				workStream <- worker.w(worker.args...)
			}()

			// For each piece of work we now create a context based on an empty one `Background` with our timeout
			// We can now listen to `Done` rather than our custom timeout and call cancel if we haven't timed out
			ctx, cancel := context.WithTimeout(context.Background(), timeout)

			select {
			case r, ok := <-workStream:
				// This is for completeness, you would usually defer this to release resources further up the chain
				cancel()
				if ok {
					rs <- workResult{
						r,
						worker.position,
						nil,
					}
				}
			case <-ctx.Done():
				rs <- workResult{
					0,
					worker.position,
					Timeout,
				}
			}
		case <-cancel:
			return
		}
	}
}

type Result struct {
	Value int
	Err   error
}

var Timeout = errors.New("Timeout")

func wrapPanic(err error) error {
	return errors.Wrap(err, "Panicked")
}

func (s *Scheduler) Run() []Result {
	var todo = make([]job, len(s.jobs))
	copy(todo, s.jobs)
	s.jobs = make([]job, 0)

	totalWork := len(todo)
	results := make([]Result, totalWork)
	workStreams := make([]workStream, 0)
	currentWorker := 0
	rs := make(resultStream, totalWork)
	cancelWorkers := make(chan struct{})

	defer close(rs)
	defer close(cancelWorkers)

	for i, j := range todo {
		if len(workStreams) < s.maxgr {
			ws := make(workStream)
			workStreams = append(workStreams, ws)
			go doWork(ws, rs, cancelWorkers, s.timeout)
		}

		if currentWorker == len(workStreams) {
			currentWorker = 0
		}

		workStreams[currentWorker] <- worker{j, i}

		currentWorker++
	}

	for i := 0; i < totalWork; i++ {
		r := <-rs
		results[r.position] = Result{
			r.value,
			r.err,
		}
	}

	return results
}
