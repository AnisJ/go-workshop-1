package part5

import "runtime"

type work func(...int) int

type job struct {
	w    work
	args []int
}

type Scheduler struct {
	maxgr int
	jobs  []job
}

func NewScheduler(maxgr int) *Scheduler {
	if maxgr == 0 {
		maxgr = runtime.NumCPU()
	}

	return &Scheduler{
		maxgr: maxgr,
	}
}

func (s *Scheduler) Add(w work, args ...int) {
	s.jobs = append(s.jobs, job{w, args})
}

type worker struct {
	job
	position int
}

type workStream chan worker

type result struct {
	value,
	position int
}

type resultStream chan result

// We don't really care what the `cancel` channel looks like, just that it's been closed
func doWork(ws workStream, rs resultStream, cancel <-chan struct{}) {
	// This is a typical for-select pattern where we continually try and match on some condition in the `select` statement
	// In this case we will try and read from the given channel and perform the work until the cancel case matches where
	// we exit
	for {
		select {
		case worker := <-ws:
			rs <- result{
				worker.w(worker.args...),
				worker.position,
			}
		// When the work has finished and we've closed the cancel channel we will exit the goroutine
		case <-cancel:
			return
		}
	}
}

func (s *Scheduler) Run() []int {
	totalWork := len(s.jobs)
	results := make([]int, totalWork)
	workStreams := make([]workStream, 0)
	currentWorker := 0
	rs := make(resultStream, totalWork)
	cancelWorkers := make(chan struct{})

	defer close(rs)
	// By closing the `cancelWorkers` channel it sends a sentinel value to let our goroutines know they can now exit
	// Reading a closed channel will return the zero value for the channel type but typically you throw this away
	defer close(cancelWorkers)

	for i, j := range s.jobs {
		if len(workStreams) < s.maxgr {
			ws := make(workStream)
			workStreams = append(workStreams, ws)
			go doWork(ws, rs, cancelWorkers)
		}

		if currentWorker == len(workStreams) {
			currentWorker = 0
		}

		workStreams[currentWorker] <- worker{j, i}

		currentWorker++
	}

	for i := 0; i < totalWork; i++ {
		r := <-rs
		results[r.position] = r.value
	}

	return results
}
